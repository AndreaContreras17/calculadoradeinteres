/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author andre
 */
public class Calculador {

    /**
     * @return the Num1
     * 
     * 
     */
    
    private int Num1;
    private int Num2;
    private int Num3;
    private int resultado;
    
    public int getNum1() {
        return Num1;
    }

    /**
     * @param Num1 the Num1 to set
     */
    public void setNum1(int Num1) {
        this.Num1 = Num1;
    }

    /**
     * @return the Num2
     */
    public int getNum2() {
        return Num2;
    }

    /**
     * @param Num2 the Num2 to set
     */
    public void setNum2(int Num2) {
        this.Num2 = Num2;
    }

    /**
     * @return the Num3
     */
    public int getNum3() {
        return Num3;
    }

    /**
     * @param Num3 the Num3 to set
     */
    public void setNum3(int Num3) {
        this.Num3 = Num3;
    }

    /**
     * @return the resultado
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    
    
   public void calcular(int num1, int num2, int num3){
       this.resultado= num1 * num2 * num3;
       
   }
        
    
    
    
}
