<%-- 
    Document   : resultado
    Created on : 09-04-2021, 16:43:16
    Author     : andre
--%>

<%@page import="modelo.Calculador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Calculador calcu=(Calculador)request.getAttribute("Calculador");

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1><%=calcu.getResultado()%></h1>
    </body>
</html>
